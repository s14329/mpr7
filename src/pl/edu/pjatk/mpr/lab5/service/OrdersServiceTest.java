package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.Address;
import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;
import pl.edu.pjatk.mpr.lab5.model.OrderItem;
import pl.edu.pjatk.mpr.lab5.service.OrdersService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static junit.framework.Assert.assertEquals;

public class OrdersServiceTest {
    
    private ClientDetails client1;
    private ClientDetails client2;
    private ClientDetails client3;
    private ClientDetails client4;

    private Address address1;
    private Address address2;
    private Address address3;
    private Address address4;

    private OrderItem orderItem1;
    private OrderItem orderItem2;
    private OrderItem orderItem3;
    private OrderItem orderItem4;
    private OrderItem orderItem5;
    private OrderItem orderItem6;
    private OrderItem orderItem7;
    private OrderItem orderItem8;

    private List<OrderItem> orderItems1;
    private List<OrderItem> orderItems2;
    private List<OrderItem> orderItems3;
    private List<OrderItem> orderItems4;
    private List<OrderItem> orderItems5;
    private List<OrderItem> orderItems6;

    private Order order1;
    private Order order2;
    private Order order3;
    private Order order4;
    private Order order5;
    private Order order6;

    private List<Order> orders;
    
    public void createTestData() {
        client1 = new ClientDetails(1, "jkowalski", "Jan", "Kowalski", 21);
        client2 = new ClientDetails(2, "jiksinski", "Janusz", "Iksiński", 22);
        client3 = new ClientDetails(3, "jzielinski", "Jacek", "Zieliński", 23);
        client4 = new ClientDetails(4, "jczerwinski", "Jerzy", "Czerwiński", 24);

        address1 = new Address(1, "Bajeczna", "5", "1", "11-111", "Gdańsk", "Polska");
        address2 = new Address(2, "Kolorowa", "4", "2", "11-111", "Sopot", "Polska");
        address3 = new Address(3, "Czarna", "3", "3", "11-111", "Gdynia", "Polska");
        address4 = new Address(4, "Biała", "2", "4", "11-111", "Żukowo", "Polska");

        orderItem1 = new OrderItem(1, "Celebration Day", "Led Zeppelin", 11.99);
        orderItem2 = new OrderItem(2, "A Night At The Opera", "Queen", 20);
        orderItem3 = new OrderItem(3, "A Day At The Races", "Queen", 9.99);
        orderItem4 = new OrderItem(4, "Starboy", "The Weekend",15.99);
        orderItem5 = new OrderItem(5, "A Pentatonix Christmas", "Pentatonix",16.99);
        orderItem6 = new OrderItem(6, "24K Magic", "Bruno Mars",17.99);
        orderItem7 = new OrderItem(7, "Moana", "Soundtrack",18.99);
        orderItem8 = new OrderItem(8, "Trolls", "Soundtrack",50.99);

        orderItems1 = Arrays.asList(orderItem1, orderItem2, orderItem3, orderItem4, orderItem5);
        orderItems2 = Arrays.asList(orderItem2, orderItem3, orderItem4, orderItem5, orderItem6);
        orderItems3 = Arrays.asList(orderItem3, orderItem4, orderItem5, orderItem6, orderItem7);
        orderItems4 = Arrays.asList(orderItem4, orderItem5, orderItem6, orderItem7, orderItem8);
        orderItems5 = Arrays.asList(orderItem5, orderItem6, orderItem7, orderItem8, orderItem1);
        orderItems6 = Arrays.asList(orderItem6, orderItem7, orderItem8, orderItem1, orderItem2);

        order1 = new Order(1, client1, address1, orderItems1, "Zamówienie nr1");
        order2 = new Order(2, client2, address2, orderItems2, "Zamówienie nr2");
        order3 = new Order(3, client3, address3, orderItems3, "Zamówienie nr3");
        order4 = new Order(4, client4, address4, orderItems4, "Zamówienie nr4");
        order5 = new Order(5, client1, address1, orderItems5, "Zamówienie nr5");
        order6 = new Order(6, client2, address2, orderItems6, "Zamówienie nr6");

        orders = Arrays.asList(order1, order2, order3, order4, order5, order6);
    }
    
    public void testFindOrdersWhichHaveMoreThan5OrderItems() {

        List<Order> expectedOrders = Arrays.asList(order1, order2, order4);

        List<Order> ordersMoreThan5streams = OrdersService.findOrdersWhichHaveMoreThan5OrderItems(orders);

        assertEquals(expectedOrders, ordersMoreThan5streams);
    }

    public void testFindOldestClientAmongThoseWhoMadeOrders() {

        ClientDetails expectedOldestClient = client2;

        ClientDetails oldestClient = OrdersService.findOldestClientAmongThoseWhoMadeOrders(orders);

        assertEquals(expectedOldestClient, oldestClient);
    }

    public void testFindOrderWithLongestComments() {

        Order expectedOrderWithLongestComments = order1;

        Order orderWithLongestComments = OrdersService.findOrderWithLongestComments(orders);

        assertEquals(expectedOrderWithLongestComments, orderWithLongestComments);
    }

    public void testGetNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld() {

        String expectedNamesAbove18Years = client1.getName() + " " + client1.getSurname()
                + ", " + client2.getName() + " " + client2.getSurname()
                + ", " + client3.getName() + " " + client3.getSurname()
                + ", " + client4.getName() + " " + client4.getSurname();

        String namesAbove18Years = OrdersService
                .getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(orders);

        assertEquals(expectedNamesAbove18Years, namesAbove18Years);
    }

    public void testGroupOrdersByClient() {

        Map<ClientDetails, List<Order>> expectedOrdersByClient = new HashMap<>();

        List<Order> client1Orders = Arrays.asList(order1, order4, order5);
        List<Order> client12Orders = Arrays.asList(order2);
        List<Order> client14Orders = Arrays.asList(order3);
        List<Order> client13Orders = Arrays.asList(order6);

        expectedOrdersByClient.put(client1, client1Orders);
        expectedOrdersByClient.put(client2, client12Orders);
        expectedOrdersByClient.put(client3, client14Orders);
        expectedOrdersByClient.put(client4, client13Orders);

        Map<ClientDetails, List<Order>> ordersByClient = OrdersService.groupOrdersByClient(orders);

        assertEquals(expectedOrdersByClient, ordersByClient);
    }

    public void testGetSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA() {

        List<String> expectedOrdersItemsNamesfromOrderWithCommentA = Arrays.asList(orderItem2.getName(),
                orderItem3.getName(), orderItem4.getName(), orderItem5.getName());

        List<String> ordersItemsNamesfromOrderWithCommentA = OrdersService
                .getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(orders);

        assertEquals(expectedOrdersItemsNamesfromOrderWithCommentA, ordersItemsNamesfromOrderWithCommentA);
    }

    public void testPartitionClientsByUnderAndOver18() {

        Map<Boolean, List<ClientDetails>> expectedClientsUnderAndOver18 = new HashMap<>();

        List<ClientDetails> clientsUnder18 = Arrays.asList(client4);
        List<ClientDetails> clientsOver18 = Arrays.asList(client1, client2, client3);

        expectedClientsUnderAndOver18.put(false, clientsUnder18);
        expectedClientsUnderAndOver18.put(true, clientsOver18);

        Map<Boolean, List<ClientDetails>> clientsUnderAndOver18 = OrdersService
                .partitionClientsByUnderAndOver18(orders);

        assertEquals(expectedClientsUnderAndOver18, clientsUnderAndOver18);
    }
    
}