package pl.edu.pjatk.mpr.lab5.service;

import pl.edu.pjatk.mpr.lab5.model.ClientDetails;
import pl.edu.pjatk.mpr.lab5.model.Order;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.Comparator;

public class OrdersService {
    public static List<Order> findOrdersWhichHaveMoreThan5OrderItems(List<Order> Orders) {
        if (Orders == null) {
                return null;
        } else {
                List<Order> ordersMoreThan5streams = Orders.stream()
                         .filter(Order -> Order.getItems().size() > 5)
                         .collect(Collectors.toList());

                return ordersMoreThan5streams;
        }
    }

    public static ClientDetails findOldestClientAmongThoseWhoMadeOrders(List<Order> Orders) {
        if (Orders == null || Orders.isEmpty()) {
                return null;
        } else {
                ClientDetails oldestClient = Orders.stream()
                        .map(Order -> Order.getClientDetails())
                        .max(Comparator.comparing(ClientDetails::getAge))
                        .get();

                return oldestClient;
          }
    }

    public static Order findOrderWithLongestComments(List<Order> Orders) {
        if (Orders == null || Orders.isEmpty()) {
                return null;
        } else {
                Order orderWithLongestComments = Orders.stream()
                        .max((x,y) -> x.getComments().length() - y.getComments().length())
                        .get();

                return orderWithLongestComments;
        }
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllClientsAbove18YearsOld(List<Order> Orders) {
        if (Orders == null || Orders.isEmpty()) {
                return null;
        } else {
                String namesAbove18Years = Orders.stream()
                        .filter(Order -> Order.getClientDetails().getAge() > 17)
                        .map(Order -> Order.getClientDetails().getName() + " " + Order.getClientDetails().getSurname())
                        .distinct()
                        .collect(Collectors.joining(", "));

                return namesAbove18Years;
        }
    }

    public static List<String> getSortedOrderItemsNamesOfOrdersWithCommentsStartingWithA(List<Order> Orders) {
        if (Orders == null) {
                return null;
        } else {
                List<String> ordersItemsNamesfromOrderWithCommentA = Orders.stream()
                        .filter(Order -> Order.getComments().startsWith("A"))
                        .flatMap(Order -> Order.getItems().stream())
                        .map(Item -> Item.getName())
                        .distinct()
                        .sorted()
                        .collect(Collectors.toList());

                return ordersItemsNamesfromOrderWithCommentA;
        }
    }

    public static void printCapitalizedClientsLoginsWhoHasNameStartingWithS(List<Order> Orders) {
        Orders.stream()
                .filter(order -> order.getClientDetails().getName().startsWith("S"))
                .map(order -> order.getClientDetails().getLogin().toUpperCase())
                .forEach(System.out::println);
    }

    public static Map<ClientDetails, List<Order>> groupOrdersByClient(List<Order> Orders) {
        if (Orders == null) {
                return null;
        } else {
                Map<ClientDetails, List<Order>> ordersByClient = Orders.stream()
                        .collect(Collectors.groupingBy(Order::getClientDetails));

                return ordersByClient;
        }
    }

    public static Map<Boolean, List<ClientDetails>> partitionClientsByUnderAndOver18(List<Order> Orders) {
        if (Orders == null) {
                return null;
        } else {
                Map<Boolean, List<ClientDetails>> clientsUnderAndOver18 = Orders.stream()
                        .map(order -> order.getClientDetails())
                        .distinct()
                        .collect(Collectors.partitioningBy(clientDetails -> clientDetails.getAge() > 17));

                return clientsUnderAndOver18;
        }
    }
}